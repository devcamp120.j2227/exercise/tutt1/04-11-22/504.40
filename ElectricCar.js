import Car from './Car.js';

class ElectricCar extends Car {
    constructor(make, model, batteryLevel) {
        super(make, model);
        this.batteryLevel = batteryLevel;
    }
   charge() {
    console.log(this.batteryLevel);
   }
}

export default ElectricCar;