import Car from './Car.js';
import ElectricCar from './ElectricCar.js';
import PetrolCar from './PetrolCar.js';

let car = new Car("ABC", "Charly");
car.drive();
console.log(car instanceof Car);

let eCar = new ElectricCar("DEF", "Cub", 5);
eCar.drive();
eCar.charge();
console.log(eCar instanceof Car);

let pCar = new PetrolCar("CON", "Card", 5);
pCar.drive();
pCar.fillUp();
console.log(pCar instanceof Car);