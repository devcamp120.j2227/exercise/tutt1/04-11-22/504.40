import Car from './Car.js';

class PetrolCar extends Car {
    constructor(make, model, fuelLevel) {
        super(make, model);
        this.fuelLevel = fuelLevel;
    }
    fillUp() {
        console.log(this.fuelLevel);
    }
}

export default PetrolCar;